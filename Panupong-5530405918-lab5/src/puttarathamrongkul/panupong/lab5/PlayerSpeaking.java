package puttarathamrongkul.panupong.lab5;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab5
 * 1. To learn how to use UML diagram
 * 2. To learn about inheritance
 * 3. To learn about polymorphism
 * 4. To learn how to use Abstract classes
 */
public class PlayerSpeaking {
	//to receive arguments to an array and print it out.
	static int numPlayers = 3;
	public static void main(String[] args) {
		Player[] players = new Player[numPlayers];
		players[0] = new ThaiPlayer("����� ������", 7, 7, 1985, 57, 169, "Best Setters");
		players[1] = new AmericanPlayer("Jeremy Lin", 2, 12, 1988, 91, 191, "https://www.facebook.com/jeremylin17");
		players[2] = new ThaiPlayer("�Ѫ�� �Թ�����", 13, 6, 1990, 50, 160);
		for(int i = 0; i < numPlayers; i++) {
			System.out.print(players[i].getName() + " ");
			players[i].speak();
		}
	}
}