package puttarathamrongkul.panupong.lab5;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab5
 * 1. To learn how to use UML diagram
 * 2. To learn about inheritance
 * 3. To learn about polymorphism
 * 4. To learn how to use Abstract classes
 */
//to inherit inheritance from super class for use.
public class ThaiPlayer extends Player {
	public ThaiPlayer(String n, int bD, int bM, int bY, double w, double h) {
		super(n, bD, bM, bY, w, h);
	}
	
	private String award = null;
//to receive award
	public ThaiPlayer(String n, int bD, int bM, int bY, double w, double h, String aw) {
		super(n, bD, bM, bY, w, h);
		award = aw;
	}

	 public String getAward() {
         return award;
	 }
	 
	 public void setAward(String newaward) {
         this.award = newaward;
	 }
	 //to override speak method
	 @Override
	 public void speak() {
			System.out.println("�ٴ������");
		}
}