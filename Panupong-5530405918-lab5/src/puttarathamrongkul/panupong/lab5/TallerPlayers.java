package puttarathamrongkul.panupong.lab5;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab5
 * 1. To learn how to use UML diagram
 * 2. To learn about inheritance
 * 3. To learn about polymorphism
 * 4. To learn how to use Abstract classes
 */
public class TallerPlayers {
	//to fill parameter 
	public static void main(String[] args){
		ThaiPlayer nusara = new ThaiPlayer("����� ������", 7, 7, 1985, 57, 169, "Best Setters");
		AmericanPlayer jeremy = new AmericanPlayer("Jeremy Lin", 2, 12, 1988, 91, 191, "https://www.facebook.com/jeremylin17");
		ThaiPlayer ratchanok = new ThaiPlayer("�Ѫ�� �Թ�����", 13, 6, 1990, 50, 160);
		isTaller(ratchanok, jeremy);
		isTaller(jeremy, nusara);
	}
	//to compare height of two player
	public static void isTaller(Player p1, Player p2){
		if(p1.getHeight() > p2.getHeight())
			System.out.println(p1.getName() + " is taller than " + p2.getName());
		else
			System.out.println(p1.getName() + " is not taller than " + p2.getName());
	}
}