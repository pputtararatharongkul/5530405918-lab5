package puttarathamrongkul.panupong.lab5;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab5
 * 1. To learn how to use UML diagram
 * 2. To learn about inheritance
 * 3. To learn about polymorphism
 * 4. To learn how to use Abstract classes
 */
public class AmericanPlayer extends Player {
	//to inherit inheritance from super class for use.
	public AmericanPlayer(String n, int bD, int bM, int bY, double w, double h) {
		super(n, bD, bM, bY, w, h);
		
	}
	private String facebookAcct;
	public AmericanPlayer(String n, int bD, int bM, int bY, double w, double h, String fb) {
		super(n, bD, bM, bY, w, h);
		facebookAcct = fb;
	}
	public String getFacebookAcct() {
		return facebookAcct;
	}
	public void setFacebookAcct(String newfacebookAcct) {
		this.facebookAcct = newfacebookAcct;
	}

}
