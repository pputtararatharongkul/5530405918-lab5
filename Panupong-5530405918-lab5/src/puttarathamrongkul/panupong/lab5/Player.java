package puttarathamrongkul.panupong.lab5;
//import puttarathamrongkul.panupong.lab4;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab5
 * 1. To learn how to use UML diagram
 * 2. To learn about inheritance
 * 3. To learn about polymorphism
 * 4. To learn how to use Abstract classes
 */
//to inherit inheritance from super class for use.
public class Player extends puttarathamrongkul.panupong.lab4.Player {

	public Player(String n, int bD, int bM, int bY, double w, double h) {
		super(n, bD, bM, bY, w, h);
	}
	//to declare default's value of language
	String language = " English";
	public Player(String n, int bD, int bM, int bY, double w, double h, String ln) {
		super(n, bD, bM, bY, w, h);
		this.language = ln;
	}
	public void speak() {
		System.out.println("Speak" + language);
	}
}